import cherrypy
from sce_session import SCESession
import datetime
import main
import pandas


class StringGenerator(object):
    def __init__(self):
        self.df = pandas.DataFrame(columns=['timestamp', 'power', 'timestamp_end'])
        self.df.set_index('timestamp', inplace=True)

    @cherrypy.expose
    def index(self):
        return """<html>
          <head></head>
          <body>
            <form method="get" action="get_data">
              <input type="text" value="griessbaum@gmail.com" name="username" />
              <br>
              <input type="text" value="PaulFucks1" name="password" /> 
              <br>
              <input type="text" value="2017-12-12" name="start_date" />
              <br>
              <input type="text" value="2017-12-14" name="end_date" />
              <br>              
              <button type="submit", name="html">Show</button>
              <button type="submit", name="csv">Download as CSV</button>
            </form>
          </body>
        </html>"""

    def download_data(self, username, password, start_date, end_date):
        session = SCESession(username=username, password=password)
        session.login()
        start_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(end_date, '%Y-%m-%d')
        self.df = main.get_range(start_date=start_date, end_date=end_date, session=session)

    @cherrypy.expose
    def csv(self):
        cherrypy.response.headers['Content-Type'] = 'text/csv'
        cherrypy.response.headers['Content-Disposition'] = 'filename=sce.csv'
        return self.df.to_csv()

    @cherrypy.expose
    def get_data(self, username, password, start_date, end_date, **kwargs):
        self.download_data(username, password, start_date, end_date)
        if "html" in kwargs:
            return self.df.to_html(columns=['power'])
        elif "csv" in kwargs:
            cherrypy.response.headers['Content-Type'] = 'text/csv'
            cherrypy.response.headers['Content-Disposition'] = 'filename=sce.csv'
            return self.df.to_csv()





if __name__ == '__main__':
    cherrypy.quickstart(StringGenerator())

