import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.dates
import numpy
import math

# Configure to always fore latex and siuintx
plt.rcParams['text.latex.preamble'] = [
    r'\usepackage{siunitx}',    # i need upright \micro symbols, but you need...
    r'\sisetup{detect-all}',    # ...this to force siunitx to actually use your fonts
    r'\usepackage{helvet}',     # set the normal font here
    r'\usepackage{sansmath}',   # load up the sansmath so that math -> helvet
    r'\sansmath'  # <- tricky! -- gotta actually tell tex to use!
]

plt.rc('text', usetex=True)
font_size = 10


class Plot:
    def __init__(self):
        width = 600.0 * 0.0138889   # pt
        height = width / (1 + 5**0.5) * 2
        self.fig, self.ax = plt.subplots(nrows=1, ncols=1, figsize=(width, height), dpi=300)
        plt.subplots_adjust(left=0.15, bottom=0.20, right=0.9, top=None, wspace=None, hspace=None)
        self.ax2 = None
        self.setup_plot()
        self.legend = []
        self.legend_loc = None
        self.lines = []
        self.marker_size = None
        
    def set_marker_size(self, marker_size):
        self.marker_size = marker_size

    def plot(self, x, y, series_name, linestyle='solid', marker=None):
        line = self.ax.plot(x, y, linestyle=linestyle, marker=marker, markersize=self.marker_size)[0]
        self.lines.append(line)
        self.legend.append(series_name)
        self.set_xlim(min(x), max(x))

    def plot_pandas(self, series, linestyle='solid', marker=None, series_name=None, linewidth=1):
        line = self.ax.plot(series, linestyle=linestyle, marker=marker, markersize=self.marker_size, linewidth=linewidth)[0]
        self.x_label(series.index.name)
        self.lines.append(line)
        self.legend.append(series.name)

    def setup_plot(self):
        self.ax.grid('on')
        self.ax.tick_params(labelsize=font_size, pad=10)

    def add_arrows(self):
        xmin, xmax = self.ax.get_xlim()
        ymin, ymax = self.ax.get_ylim()
        hw = 1. / 20. * (ymax - ymin)
        hl = 1. / 20. * (xmax - xmin)
        lw = 1.  # axis line width
        ohg = 0.3  # arrow overhang
        dps = self.fig.dpi_scale_trans.inverted()
        bbox = self.ax.get_window_extent().transformed(dps)
        width, height = bbox.width, bbox.height
        yhw = hw / (ymax - ymin) * (xmax - xmin) * height / width
        yhl = hl / (xmax - xmin) * (ymax - ymin) * width / height
        self.ax.arrow(xmin, ymin, 1.03 * (xmax - xmin), 0., fc='k', ec='k', lw=lw,
                      head_width=hw, head_length=hl, overhang=ohg,
                      length_includes_head=False, clip_on=False)
        self.ax.arrow(xmin, ymin, 0., 1.03 * (ymax - ymin), fc='k', ec='k', lw=lw,
                      head_width=yhw, head_length=yhl, overhang=ohg,
                      length_includes_head=False, clip_on=False)

    def show(self):
        self.make_legend()
        plt.show()

    def save_fig(self, filename):
        self.make_legend()
        plt.savefig(filename)

    def x_label(self, x_label):
        self.ax.set_xlabel(x_label, fontsize=font_size, labelpad=20)
        self.ax.xaxis.set_label_coords(0.9, -0.13)

    def y_label(self, y_label):
        self.ax.set_ylabel(y_label, fontsize=font_size, labelpad=20, rotation=0)
        self.ax.yaxis.set_label_coords(-0.15, 0.87)

    def y_label2(self, y_label):
        self.ax2.set_ylabel(y_label, fontsize=font_size, labelpad=20, rotation=0)
        self.ax2.yaxis.set_label_coords(1.1, 0.9)

    def make_legend(self):
        self.ax.legend(handles=self.lines, labels=self.legend, fontsize=font_size, loc=self.legend_loc)

    def set_legend_loc(self, location):
        self.legend_loc = location

    def title(self, title):
        self.ax.set_title(title, fontsize=font_size)

    def set_xlim(self, x_min, x_max):
        self.ax.set_xlim([x_min, x_max])

    def set_ylim(self, y_min, y_max):
        self.ax.set_ylim([y_min, y_max])

    def vertical_line(self, x, label):
        self.ax.axvline(x=x, ymin=0, ymax=50, color='r', label=label)

    def set_xoffset(self, offset):
        self.ax.get_xaxis().get_major_formatter().set_useOffset(offset)

    def close(self):
        plt.close(self.fig)

    def set_date_axis_format(self, date_format):
        self.ax.xaxis.set_major_formatter(matplotlib.dates.DateFormatter(date_format))
        
    def set_axis_timezone(self, tz):
        plt.gca().xaxis_date(tz)

    def set_daily_ticks(self):
        self.ax.xaxis.set_major_locator(matplotlib.dates.DayLocator())

    def add_text_ticks(self, labels):
        plt.xticks(numpy.arange(len(labels)), labels)
        
    def rotate_ticks(self, rotation):
        plt.xticks(rotation=rotation)

    def plot_second_y(self, x, y, series_name):
        self.ax2 = self.ax.twinx()
        self.ax2.plot(x, y, '-')
        self.legend.append(series_name)

    def add_second_y(self):
        self.ax2 = self.ax.twinx()
        self.ax2.tick_params(labelsize=font_size, pad=10)
        plt.subplots_adjust(right=0.85)
        #self.ax2.set_prop_cycle = self.ax._get_lines.prop_cycler    # Set the cycler to the same state
        for line in self.lines:
            next(self.ax2._get_lines.prop_cycler)
            
    def plot_second_y(self, x, y, series_name):
        if not self.ax2:
            self.add_second_y()
        line = self.ax2.plot(x, y, '-')[0]
        self.lines.append(line)
        self.legend.append(series_name)

    def plot_pandas_second_y(self, series, linestyle='-', marker='None'):
        if not self.ax2:
            self.add_second_y()
        line = self.ax2.plot(series, linestyle=linestyle, marker=marker, markersize=self.marker_size)[0]
        self.lines.append(line)
        self.legend.append(series.name)

    def scientific_notation(self, x=True, y=True):
        if x:
            self.ax.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        if y:
            self.ax.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    
    def close(self):
        matplotlib.pyplot.close(self.fig)


if __name__ == '__main__':
    import nvector
    a = nvector.GeoPoint(10, 10, degrees=True)
    b = nvector.GeoPoint(20, 20, degrees=True)
    path = nvector.GeoPath(a, b)
    xs = []
    ys = []
    zs = []
    n_steps = 5
    for step in range(n_steps + 1):
        pvector = path.interpolate(step / n_steps).to_ecef_vector().pvector
        xs.append(pvector[0][0])
        ys.append(pvector[1][0])
        zs.append(pvector[2][0])
    plt = SpherePlot(radius=10)

