# sce_downloader
This is a set of scripts do download and analyze smartmeter 
consumption data from the southern california website.


# Requirements
Python3
SQLAlchemy 1.1.11
Pandas 0.22.0
latex
texlive-latex-extra/bionic


apt install python3 python3-sqlalchemy python3-pandas latex texlive-latex-extra
