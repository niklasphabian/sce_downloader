import datetime
import database
import time
import pandas
from sce_session import SCESession
import configparser


db = database.SQLiteDatabase('power.sqlite')
power_table = database.PowerTable(db)


def read_credentials():
    config = configparser.ConfigParser(allow_no_value=True)
    config.optionxform = str        
    config.read('credentials.ini')
    config_name = config.sections()[0]
    username = config[config_name]['username']
    password = config[config_name]['password']
    return username, password


def get_range(start_date, end_date, session):
    date = start_date
    range_df = pandas.DataFrame(columns=['timestamp', 'power', 'timestamp_end'])
    range_df.set_index('timestamp', inplace=True)
    while date < end_date:
        print(date)
        day_df = get_day(date, session)
        power_table.upsert_dataframe(day_df)
        date += datetime.timedelta(days=1)
        range_df = range_df.append(day_df)
        time.sleep(0.5)
    return range_df


def get_day(date, session):
    n_fail = 0
    while True:
        df = session.get_date_as_df(date)
        if len(df.index) > 0:
            break
        elif n_fail > 5:
            print('tried 5 times skipping date')
            break
        elif n_fail > 3:
            print('reconnecting')
            session.reconnect()
            n_fail += 1
        else:
            print('dl failed')
            n_fail += 1
            time.sleep(5)
    return df


if __name__ == '__main__':
    username, password = read_credentials()
    session = SCESession(username=username, password=password)
    session.login()        
    start_date = power_table.latest_entry().replace(hour=0, minute=0)
    #start_date = datetime.datetime(2017, 6, 30)
    end_date = datetime.datetime.now() - datetime.timedelta(days=2)    
    df = get_range(start_date=start_date, end_date=end_date, session=session)
    power_table.upsert_dataframe(df)
