import requests
import time
import sys
import datetime
from bs4 import BeautifulSoup
import pandas


class SCESession:

    def __init__(self, username, password):
        self.session = requests.Session()
        self.username = username
        self.password = password
        self.service_account_number = None
        self.account_number = None
        self.time_since_last_request = datetime.datetime(2000, 1, 1)

    def verify_waiting_time(self):
        waited = (datetime.datetime.now() - self.time_since_last_request).total_seconds()
        if waited < 1:
            time.sleep(1 - waited)
        self.time_since_last_request = datetime.datetime.now()

    def get(self, url, data=None):
        self.verify_waiting_time()
        failed = True
        while failed:
            try:
                ret = self.session.get(url=url, data=data)
                failed = False
            except requests.exceptions.ConnectionError:
                print('Connection Error. Reconnecting')
                self.reconnect()
        return ret

    def post(self, url, data=None):
        self.verify_waiting_time()
        failed = True
        while failed:
            try:
                ret = self.session.post(url=url, data=data)
                failed = False
            except requests.exceptions.ConnectionError:
                print('Connection Error. Reconnecting')
                self.reconnect()
        return ret

    def reconnect(self):
        self.logout()
        self.session = requests.Session()
        self.login()

    def logout(self):
        ret = self.get(url='https://www.sce.com/wps/portal/home/mysce/logout')
        url = ret.headers['content-location']
        self.get(url=url)
        self.get(url='https://www.sce.com/pkmslogout')

    def pre_login(self):
        url = 'https://www.sce.com/wps/portal/home/mysce/login/!ut/p/b1/jc_RCoIwFAbgZ-kB4hxcCF6uEHMwTTRau4kRNge6iY2gt0-hmy7Uzt2B7__hBwkCpFUvo5U3zqp2-mV4ixKMjyzHNK94hCmLQ7aL9gHGZATXEeDMUfwvPwc4WctfQC6SqeEXJFVBMCUFZiWlBDH8goUN2dF1NbA1Nm4JBn7gGmSvfLM19uFAlF4NHkT3ft5rEK3TxkLfnQWaU9NquvkAWL2DWA!!/pw/Z7_18GAHIS0IGE5A0IB09T5L630I2/res/c=cacheLevelPage/=/'
        data = {'loginEmailID': self.username,
                'oktaURL': 'NA',
                'password': self.password,
                'targetURL': 'https://www.sce.com/wps/portal/home/!ut/p/b1/04_Sj9CPykssy0xPLMnMz0vMAfGjzOIt3Q1cPbz8DTzdQwKNDTyNAw38gh0djQ0MzIAKIoEKDHAARwNC-sP1o8BK8JhQkBthkO6oqAgAStf4Iw!!/dl4/d5/L2dBISEvZ0FBIS9nQSEh/'}
        self.post(url=url, data=data)
        data = {'oktaURL': 'ourl'}
        ret = self.post(url=url, data=data)
        print(ret.text)
        if ret.status_code == 200:
            overview_page_url = ret.headers['content-location']
            return overview_page_url
        else:
            error_msg = 'Status code {}. Password may be incorrect'
            error_msg = error_msg.format(ret.status_code)
            print(error_msg)
            sys.exit()

    def login(self):
        self.pre_login()
        self.get_service_account_number()
        self.download_usage_page()

    def download_account_page(self):
        url = 'https://www.sce.com/SMA/ESCAA/EscMyAccount.aspx'
        ret = self.get(url=url)
        return ret

    def get_service_account_number(self):
        ret = self.download_account_page()
        soup = BeautifulSoup(ret.text, 'lxml')
        service_id = 'ctl00_ctl00_ctl00_EscBaseMasterCentralPlace_CentralContentPlace_CentralContentPlaceTab_HiddenServAcctNumOrcat'
        self.service_account_number = soup.find('input', id=service_id)['value']
        return self.service_account_number

    def get_account_number(self):
        ret = self.download_account_page()
        soup = BeautifulSoup(ret.text, 'lxml')
        account_id = 'ctl00_ctl00_ctl00_EscBaseMasterCentralPlace_LoginBoxPlace_LoginControl_AccountNumber'
        self.account_number = soup.find('span', id=account_id).contents[0]
        return self.account_number

    def download_usage_page(self):
        ret = self.get(url='https://www.sce.com/SMA/ESCAA/UsageHourly.aspx')
        return ret

    def get_average_usage(self):
        ret = self.download_usage_page()
        soup = BeautifulSoup(ret.text, 'lxml')
        usage_id = 'ctl00_ctl00_ctl00_EscBaseMasterCentralPlace_CentralContentPlace_CentralContentPlaceInnerTab_Usage'
        avg_use = soup.find('span', id=usage_id).contents[0]
        return avg_use

    def set_date(self, date):
        self.session.headers.update({'SOAPAction': None})
        self.session.headers.update({'Content-Type': None})
        self.session.headers.update({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'})
        date_selector = '%3CDateChooser%20Value%3D%22{year}x{month}x{day}%22%3E%3C/DateChooser%3E'
        date_selector = date_selector.format(year=date.year, month=date.month, day=date.day)
        payload = {
            '__VIEWSTATE': '/wEPDwUJNDIwMTYzMDcyD2QWAmYPZBYCZg9kFgJmD2QWCmYPZBYCAgIPFQoFL1NNQS8FL1NNQS8aRVNDL0VzY015QWNjb3VudC5qcz92PTEuMTgFL1NNQS8FL1NNQS8BWYkBQVpXVlBXRUI7WkkxTlRDTVM7WkkxVlBXRUI7Q01TMDc7Q01TMDg7Q01TMDk7Q01TMTA7Q01TMTE7Q01TMTI7V0VCMDE7V0VCMDI7V0VCMDM7V0VCMDQ7V0VCMDU7V0VCMDY7V0VCMDc7V0VCMDg7V0VCMDk7V0VCMTA7bHRtd3d3LnNjZS5jb201aHR0cHM6Ly93d3cuc2NlLmNvbS93cHMvbXlwb3J0YWwvaG9tZS9teXNjZS9teWFjY291bnQLd3d3LnNjZS5jb201aHR0cHM6Ly93d3cuc2NlLmNvbS93cHMvbXlwb3J0YWwvaG9tZS9teXNjZS9teWFjY291bnRkAgEPZBYCZg8PFgIeB1Zpc2libGVoZGQCAg9kFgICAw9kFg4CAQ9kFgICAw9kFgICAQ9kFgQCAQ8PZBYCHgdvbmNsaWNrBR1qYXZhc2NyaXB0OnJldHVybiBlc2NQcmludCgpO2QCBQ9kFgICAQ8WAh4JaW5uZXJodG1sBZgFPGRpdiBjbGFzcz0nZWxlbWVudENTUyc+PGEgaHJlZj0iamF2YXNjcmlwdDpPcGVuRG93bmxvYWRIVE1MKCk7IiBvbmNsaWNrPSJPcGVuRG93bmxvYWRIVE1MKCk7IiBzdHlsZT0nY29sb3I6IEJsYWNrOycgY2xhc3M9J3Rvb2xUaXBDYWxlbmRhcicgb25ibHVyPSJTaG93U2VsZWN0ZWRSb3codGhpcywnT1VUJyk7IiBvbmZvY3VzPSJTaG93U2VsZWN0ZWRSb3codGhpcywnSU4nKTsiIG9ubW91c2VvdmVyPSJTaG93U2VsZWN0ZWRSb3codGhpcywnSU4nKTsiIG9ubW91c2VvdXQ9IlNob3dTZWxlY3RlZFJvdyh0aGlzLCdPVVQnKTsiPkRvd25sb2FkIGFzIEhUTUwgVGV4dDwvYT48L2Rpdj48ZGl2IGNsYXNzPSdlbGVtZW50Q1NTJz48YSBocmVmPSJqYXZhc2NyaXB0Ok9wZW5Eb3dubG9hZFhMUygpOyIgb25jbGljaz0iT3BlbkRvd25sb2FkWExTKCk7IiBzdHlsZT0nY29sb3I6IEJsYWNrOycgY2xhc3M9J3Rvb2xUaXBDYWxlbmRhcicgb25ibHVyPSJTaG93U2VsZWN0ZWRSb3codGhpcywnT1VUJyk7IiBvbmZvY3VzPSJTaG93U2VsZWN0ZWRSb3codGhpcywnSU4nKTsiIG9ubW91c2VvdmVyPSJTaG93U2VsZWN0ZWRSb3codGhpcywnSU4nKTsiIG9ubW91c2VvdXQ9IlNob3dTZWxlY3RlZFJvdyh0aGlzLCdPVVQnKTsiPkRvd25sb2FkIGFzIFNwcmVhZHNoZWV0PC9hPjwvZGl2PmQCAw9kFgICAQ9kFgZmDxYCHgVzdHlsZQUabWFyZ2luOjBweDtkaXNwbGF5OmlubGluZTsWBAIBDxYCHwMFDWRpc3BsYXk6bm9uZTtkAgMPFgIfAGcWDAIBDxYEHwEFQWphdmFzY3JpcHQ6d2luZG93LmxvY2F0aW9uLnJlcGxhY2UoJy9zbWEvU2VjdXJlQXV0aC9Mb2dvdXQuYXNweCcpHwBoZAIDDw8WBB4EVGV4dAUMRmViIDIyLCAyMDE4HwBoZGQCBQ8PFgQfBAUIV2VsY29tZSAfAGhkZAIGDw8WBh8EBQxHUklFU1NCQVUuLi4eB1Rvb2xUaXAFHkdSSUVTU0JBVU0gIE5JS0xBUyBGICAgICAgICAgIB8AaGRkAggPZBYCAgIPDxYCHwQFDTItMzktNzk3LTgwMjRkZAIJD2QWBAIDDw8WBB8EBQhET01FU1RJQx4LTmF2aWdhdGVVcmwFJC93cHMvbXlwb3J0YWwvaG9tZS9yZXNpZGVudGlhbC9yYXRlc2RkAgUPDxYEHwRlHwBoZGQCAg8WAh8DBRhtYXJnaW46MHB4O2Rpc3BsYXk6bm9uZTtkAgcPFQFTY3RsMDBfY3RsMDBfY3RsMDBfRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZV9Mb2dpbkJveFBsYWNlX0xvZ2luQ29udHJvbF9pbWdQbHVzTWludXNkAgcPZBYCAgEPZBYCAgEPZBYEAgkPFgIfAGhkAgsPFgIfAGhkAgsPZBYCAgEPZBYCZg8WAh8AaGQCDQ9kFgICAQ9kFgJmDxYCHwBoZAIPD2QWDAIBD2QWAmYPFgIeBWNsYXNzBQ9yZW1vdmVBbGVydHNEaXZkAgMPZBYCZg8WAh8DBQ1kaXNwbGF5Om5vbmU7ZAIFD2QWAgIFD2QWAgIDDxAWAh4LXyFEYXRhQm91bmRnZBQrAQBkAg8PZBYCAgMPZBYCAgEPZBYIAgcPDxYCHwRkZGQCCQ8WAh8AaGQCCw9kFgICAQ8PFgIfBGRkZAINDw8WAh8EZGRkAhMPZBYCAgEPZBYIAgcPDxYCHwRkZGQCCQ8WAh8AaGQCCw9kFgICAQ8PFgIfBGRkZAINDw8WAh8EZGRkAhUPZBYiAgMPZBYIAgEPFgIfAgUGSG91cmx5ZAIDDxYCHwIFDEZlYiAxOCwgMjAxOGQCBQ9kFgICAQ8UKwAFZDwrAAUBABYCHgVWYWx1ZQYAgMKNYnbVCBYEHgFVBgCAwo1idtUIHgFZBgDAvLwGadUIFCsAAmRkZBYCZg9kFgJmDxQrAAJkFgQfCgYAgMKNYnbVCB8LBgDAvLwGadUIZAIHDw9kFgIeBVN0eWxlBRd3aWR0aDo3NSU7ZGlzcGxheTpub25lOxYKAgEPDxYCHwQFTChFbnRlciB0aGUgRGF0ZSBpbiBNTS9ERC9ZWVlZIGZvcm1hdDxiciAvPmJldHdlZW4gMDYvMzAvMjAxNyBBbmQgMDIvMjEvMjAxOClkZAIDDw8WAh4PQXNzb2NpYXRlZExhYmVsBWZjdGwwMF9jdGwwMF9jdGwwMF9Fc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZUlubmVyVGFiX0RhdGVQZXJpb2RkZAIFDw8WAh8NBWZjdGwwMF9jdGwwMF9jdGwwMF9Fc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZUlubmVyVGFiX0RhdGVQZXJpb2RkZAIHDw8WAh8NBWZjdGwwMF9jdGwwMF9jdGwwMF9Fc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZUlubmVyVGFiX0RhdGVQZXJpb2RkZAILDw9kFgofAwUUdGV4dC1kZWNvcmF0aW9uOm5vbmUeC29ubW91c2VvdmVyBX9TaG93R3JlZW5CdXR0b24oJ2N0bDAwX2N0bDAwX2N0bDAwX0VzY0Jhc2VNYXN0ZXJDZW50cmFsUGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlSW5uZXJUYWJfR2V0U2VsZWN0ZWREYXRlJyk7Hgpvbm1vdXNlb3V0BX9TaG93QmxhY2tCdXR0b24oJ2N0bDAwX2N0bDAwX2N0bDAwX0VzY0Jhc2VNYXN0ZXJDZW50cmFsUGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlSW5uZXJUYWJfR2V0U2VsZWN0ZWREYXRlJyk7HgdvbmZvY3VzBX9TaG93R3JlZW5CdXR0b24oJ2N0bDAwX2N0bDAwX2N0bDAwX0VzY0Jhc2VNYXN0ZXJDZW50cmFsUGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlSW5uZXJUYWJfR2V0U2VsZWN0ZWREYXRlJyk7HgZvbmJsdXIFf1Nob3dCbGFja0J1dHRvbignY3RsMDBfY3RsMDBfY3RsMDBfRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VJbm5lclRhYl9HZXRTZWxlY3RlZERhdGUnKTtkAgUPFgIfAGhkAgcPFgQfAgUeQXZlcmFnZSBIb3VybHkgVXNhZ2U6IDAuNDcga1doHwBnZAIIDxYCHwBoZAIMD2QWAgIBDw8WAh8AaGRkAg4PFgIfAGgWBgIBDxYCHwBoZAIDDw8WBB8EBStDbGljayBhbiBob3VyIHRvIHZpZXcgMTUgbWluIGludGVydmFsIHVzYWdlHwBoZGQCBQ9kFgICAQ88KwANAQAPFgQfCGceC18hSXRlbUNvdW50ZmRkAhAPFgIfAGdkAhIPFgIfAGcWAgIBDw8WBB8EBS5DbGljayBhbiBob3VyIHRvIHZpZXcgMTUgbWludXRlIGludGVydmFsIHVzYWdlHwBoZGQCFA8WAh8AaGQCFg8WAh8AZxYCAgUPFgIfAGhkAhoPFgIfAGhkAhwPFgQfAgVJPHA+WW91ciB1c2FnZSBkYXRhIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZS4gUGxlYXNlIGNoZWNrIGJhY2sgbGF0ZXIuPC9wPh8AaGQCHg9kFgQCAQ9kFgYCAQ8WAh8DBQ1kaXNwbGF5Om5vbmU7FgICAQ8PFgIfBGVkZAICDxYCHwMFDWRpc3BsYXk6bm9uZTsWAgIBDw8WAh4NT25DbGllbnRDbGljawVucmV0dXJuIE9wZW5XaW5kb3coJ2h0dHBzOi8vd3d3LmNoYXQuc2NlLmNvbS9TY2VDaGF0L0NoYXQvV2ViQ2hhdElucHV0LmFzcHgnLCdjaGF0JywneWxkczBpNDV5ZWN6NXg1NTRmZnFudzQ1JylkZAIDDxYCHwMFDWRpc3BsYXk6bm9uZTtkAgMPD2QWCh8DBRR0ZXh0LWRlY29yYXRpb246bm9uZR8OBXtTaG93R3JlZW5CdXR0b24oJ2N0bDAwX2N0bDAwX2N0bDAwX0VzY0Jhc2VNYXN0ZXJDZW50cmFsUGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlSW5uZXJUYWJfUmVjZW50VXNhZ2UnKTsfDwV7U2hvd0JsYWNrQnV0dG9uKCdjdGwwMF9jdGwwMF9jdGwwMF9Fc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZUlubmVyVGFiX1JlY2VudFVzYWdlJyk7HxAFe1Nob3dHcmVlbkJ1dHRvbignY3RsMDBfY3RsMDBfY3RsMDBfRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlX0NlbnRyYWxDb250ZW50UGxhY2VJbm5lclRhYl9SZWNlbnRVc2FnZScpOx8RBXtTaG93QmxhY2tCdXR0b24oJ2N0bDAwX2N0bDAwX2N0bDAwX0VzY0Jhc2VNYXN0ZXJDZW50cmFsUGxhY2VfQ2VudHJhbENvbnRlbnRQbGFjZV9DZW50cmFsQ29udGVudFBsYWNlSW5uZXJUYWJfUmVjZW50VXNhZ2UnKTtkAiAPFgIfCQUKMDYvMzAvMjAxN2QCIg8WAh8JBQowMi8yMS8yMDE4ZAImDxYCHwkF9AJTZXJ2aWNlQWNjb3VudE51bWJlcj00NjcxOTM2NSZDdXN0b21lckFjY291bnROdW1iZXI9Mzk3OTc4MDI0JlJlcG9ydFR5cGU9SG91cmx5JlN0YXJ0RGF0ZT0yMDE4LTAyLTE4IDEyOjAwOjAwIEFNJkVuZERhdGU9MjAxOC0wMi0xOSAxMjowMDowMCBBTSZNZWFzdXJlbWVudFR5cGU9S1dIX0RFTCZGcmVxdWVuY3k9NjAmQmlsbFZlcnNpb25JZD1mY2Y4ZGEwNy1hZDI0LTQyZTctYTQzOC03ZWU2NmJiYTRhNDImTnVtYmVyT2ZEYXlzPS0xJlppcENvZGU9OTMxMDEmQ3VycmVudERhdGU9MjAxOC0wMi0yMiZSZXNpZGVudGlhbENvbW1lcmNpYWxJbmRpY2F0b3I9UiZvblBlYWtJbmRpY2F0b3JGbGFnPU4maXNUT1VGbGFnPU4mRXN0aW1hdGVkVHlwZT1CaWxsZWRkAigPFgIfCQUBTmQCEQ9kFgQCAQ8WAh8AaGQCBQ8WAh8AaGQCAw8PFgIfAGhkZAIED2QWAgIGDxUBJWh0dHBzOi8vd3d3LnNjZS5jb20vd3BzL215cG9ydGFsL2hvbWVkGAIFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYEBVJjdGwwMCRjdGwwMCRjdGwwMCRFc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlJExvZ2luQm94UGxhY2UkTG9naW5Db250cm9sJENoa1JlbWVtYmVyBWhjdGwwMCRjdGwwMCRjdGwwMCRFc2NCYXNlTWFzdGVyQ2VudHJhbFBsYWNlJENlbnRyYWxDb250ZW50UGxhY2UkQ2VudHJhbENvbnRlbnRQbGFjZUlubmVyVGFiJERhdGVTZWxlY3RvcgV5Y3RsMDAkY3RsMDAkY3RsMDAkRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZSRDZW50cmFsQ29udGVudFBsYWNlJENlbnRyYWxDb250ZW50UGxhY2VJbm5lclRhYiREYXRlU2VsZWN0b3IkRHJwUG5sJENhbGVuZGFyMQVzY3RsMDAkY3RsMDAkY3RsMDAkRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZSRDZW50cmFsQ29udGVudFBsYWNlJENlbnRyYWxDb250ZW50UGxhY2VJbm5lclRhYiRDaGF0Q2FsbGJhY2skQ2hhdEJ1dHRvbgVvY3RsMDAkY3RsMDAkY3RsMDAkRXNjQmFzZU1hc3RlckNlbnRyYWxQbGFjZSRDZW50cmFsQ29udGVudFBsYWNlJENlbnRyYWxDb250ZW50UGxhY2VJbm5lclRhYiRVc2FnZUhvdXJseUdyaWRWaWV3DzwrAAoBCGZkT841aVYNm6RV7Z2qPAYCgwgslHk=',
            '__EVENTVALIDATION':'/wEWIAKmgfbUAwL/k93ZDQKK3Mu+BAK6u+CVDAKM98HEDQK0/vvcAwL64L2yBAKr14WRAwKZ/anhBALx6bObAQKIsKAKAquOzucDAqPP8aQLAoubgZIFAoui5PYDAtaHn+ECAsjjgPYPArGsgMQGAr3zia4KArH1y5YDAvKwyqAJAunehoENAq76q+sMAqyF08ECAtPLo4wHAoGk7+wJApyKtIUJAtLa/u0HApPKv4IFAoCz2+oBArm0hd0EAvblj8YNZyEv7h603+92MKA8uJwsk8K53mA=',
            'ctl00$ctl00$ctl00$EscBaseMasterCentralPlace$LoginBoxPlace$LoginControl$txtUser': self.username,
            'ctl00$ctl00$ctl00$EscBaseMasterCentralPlace$LoginBoxPlace$LoginControl$txtPwd': self.password,
            'ctl00$ctl00$ctl00$EscBaseMasterCentralPlace$CentralContentPlace$CentralContentPlaceInnerTab$DateSelector_hidden': date_selector,
            'ctl00$ctl00$ctl00$EscBaseMasterCentralPlace$CentralContentPlace$CentralContentPlaceInnerTab$HiddenSelectionChangeIndicator': 'Y'
        }
        url = 'https://www.sce.com/SMA/ESCAA/UsageHourly.aspx'
        self.post(url=url, data=payload)

    def get_data(self, date):
        self.session.headers.update({'SOAPAction': '"http://tempuri.org/GetUsageData"'})
        self.session.headers.update({'Content-Type': 'text/xml; charset=utf-8'})
        date_end = date + datetime.timedelta(days=1)
        payload = '''<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                        <SOAP-ENV:Body>
                            <tns:GetUsageData xmlns:tns="http://tempuri.org/">
                            <tns:integrationDvo>                            
                                <s1:ServiceAccountNumber xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">{service_account_number}</s1:ServiceAccountNumber>
                                <s1:StartDateTime xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">{start_datetime}</s1:StartDateTime>
                                <s1:EndDateTime xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">{end_datetime}</s1:EndDateTime>
                                <s1:MeasurementType xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">KWH_DEL</s1:MeasurementType>
                                <s1:NumberOfDays xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">-1</s1:NumberOfDays>
                                <s1:ReportType xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">Hourly</s1:ReportType>
                                <s1:Frequency xmlns:s1="http://sce.com/schemas/FlexIntegrationDvo">60</s1:Frequency>                                                                            
                            </tns:integrationDvo>
                            </tns:GetUsageData>
                        </SOAP-ENV:Body>
                    </SOAP-ENV:Envelope>'''
        payload = payload.format(start_datetime=date.strftime('%Y-%m-%d %H:%M:%S %p'),
                                 end_datetime=date_end.strftime('%Y-%m-%d %H:%M:%S %p'),
                                 service_account_number=self.service_account_number)
        url = 'https://www.sce.com/sma/EscMyAccountFlexIntWebService.asmx'
        ret = self.post(url=url, data=payload)
        return ret.text

    def html2df(self, html):
        soup = BeautifulSoup(html, 'xml')
        data_points = soup.find_all('EscUsageCollection')
        timestamp = []
        timestamp_end = []
        power = []
        for data_point in data_points:
            timestamp.append(datetime.datetime.strptime(data_point.find('StartDate').contents[0], '%Y-%m-%d %I:%M:%S %p'))
            timestamp_end.append(datetime.datetime.strptime(data_point.find('EndDate').contents[0], '%Y-%m-%d %I:%M:%S %p'))
            power.append(float(data_point.find('Usage').contents[0]))
        df = pandas.DataFrame(data={'timestamp': timestamp, 'power': power, 'timestamp_end': timestamp_end})
        df.set_index(keys='timestamp', inplace=True)
        return df

    def get_date_as_df(self, date):
        self.set_date(date)
        html = self.get_data(date)
        return self.html2df(html)


if __name__ == '__main__':
    username = ''
    password = ''
    session = SCESession(username=username, password=password)

    date = datetime.datetime(2017, 10, 19)
    session.pre_login()
    session.get_service_account_number()
    session.download_usage_page()
    session.set_date(date=date)
    html = session.get_data(date=date)
    session.logout()
