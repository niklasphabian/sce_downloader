import database
import datetime
import plots
import os


min_date = datetime.datetime(2017, 1, 1)
db = database.SQLiteDatabase('power.sqlite')
power_table = database.PowerTable(db)


def plot_full_dataset():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_dataframe()
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'], linewidth=0.1)    
    x_max = power_table.latest_entry()
    x_min = power_table.earliest_entry()    
    plot.set_xlim(x_min, x_max)
    plot.rotate_ticks(90)
    plot.x_label(x_label='')
    plot.save_fig('figures/full.png')


def plot_day(date):
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_day_dataframe(date=date)
    df.sort_index(inplace=True)    
    plot.plot_pandas(df['power'], marker='.')
    plot.set_xlim(0, 23)    
    plot.x_label(x_label='')
    plot.save_fig('figures/days/{date}.png'.format(date=date))
    plot.close()

    
def plot_week(year, week_number):
    week_number = str(week_number).zfill(2)
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_week_dataframe(year, week_number)
    df.sort_index(inplace=True)        
    plot.plot_pandas(df['power'], marker='.')
    plot.x_label(x_label='')
    x_max = df.index[-1]
    x_min = df.index[0]
    plot.set_xlim(x_min, x_max)
    plot.rotate_ticks(90)
    plot.save_fig('figures/weeks/{year}-{week}.png'.format(year=year, week=week_number))
    plot.close()


def plot_all_days():
    first_date = power_table.earliest_entry().date()
    last_date = power_table.latest_entry().date()
    date = first_date
    while date <= last_date:
        if not os.path.isfile('figures/days/{date}.png'.format(date=date)):
            plot_day(date)
        date += datetime.timedelta(days=1)        


def plot_all_weeks():
    first_date = power_table.earliest_entry().date()
    last_date = power_table.latest_entry().date()
    date = first_date
    while date <= last_date:
        year = date.isocalendar()[0]
        week_number = date.isocalendar()[1]
        if not os.path.isfile('figures/weeks/{year}-{week}.png'.format(year=year, week=week_number)):
            plot_week(year, week_number)
        date += datetime.timedelta(days=7)          


def plot_daily():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_daily_dataframe(min_date=min_date)
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'], marker='.')
    x_max = power_table.latest_entry().date()
    x_min = power_table.earliest_entry().date()
    plot.set_xlim(x_min, x_max)
    plot.rotate_ticks(90)
    plot.x_label(x_label='')
    plot.save_fig('figures/daily.png')


def plot_weekly():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_weekly_dataframe(min_date=min_date)
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'], marker='.')
    plot.x_label(x_label='')    
    x_max  = df.index[-1]
    x_min  = df.index[0]
    plot.set_xlim(x_min, x_max)
    plot.rotate_ticks(90)
    plot.save_fig('figures/weekly.png')


def plot_monthly():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_monthly_dataframe(min_date=min_date)
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'], marker='.')
    plot.x_label(x_label='')    
    x_max  = df.index[-1]
    x_min  = df.index[0]
    plot.set_xlim(x_min, x_max)
    plot.rotate_ticks(90)
    plot.save_fig('figures/monthly.png')


def plot_hours_of_the_day():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_hour_dataframe()
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'])
    plot.set_xlim(0, 23)
    plot.save_fig('figures/hourly.png')


def plot_days_of_the_week():
    plot = plots.Plot()
    plot.y_label('Energy \n in \si{\kilo\watt\hour  }')
    df = power_table.to_weekday_dataframe()
    df.sort_index(inplace=True)
    plot.plot_pandas(df['power'])
    plot.add_text_ticks(labels=df['daystring'].tolist())
    plot.set_xlim(0, 6)
    plot.save_fig('figures/weekday.png')


if __name__ == '__main__':
    plot_all_weeks()
    plot_all_days()
    plot_full_dataset()
    plot_hours_of_the_day()
    plot_daily()
    plot_weekly()
    plot_monthly()
    plot_days_of_the_week()
